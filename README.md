# Résolution des problèmes et ontologie des connaissances

**3D printer :** *U30 pro, LK4 pro, Diggro3* 

This guide will attempt to make a typologie of troubleshoot for this 3D printer and to build a solid knowledge ontolgy and basis about this printer.

This guide should become open sourced.


## Sommaire général :
- [Pièces et modules](#pi%C3%A8ces-et-modules)
	- [Mécanique](#mécanique)
	- [Electronique](#electronique)
	- [informatique](#informatique)
- [Problèmes récurrents](#problèmes-récurents) 
- [Evolutions possible](#evolutions-possible)


**Usefull links :**

 - https://www.lesimprimantes3d.fr/forum/topic/26318-alfawise-u30-pro-r%C3%A9cap-liens-et-ressources-wip/
 - https://www.facebook.com/groups/1708618302613729/

<!-- Pièces et modules -->

## Pièces et modules
- Imprimante de type FDM (x,y,z,e)
- standalone
- Plateau d'impression amovible en Y
- Mono-axe en Z
- Transmission des axes par courroies et roues 

---

### Mécanique
#### Structure
Aluminium V-Slot
(mesure à faire)
- 2x barres en double U (side)
- 1x barre en U (top)
- 3x barre en double U (support à plat)
- 1x barre en double U (axe Y)
- 1x barre en simple U (axe X)
- 4x barre en double U (pieds)

#### Lit d'impression
Print size 220x220x250

#### Rouee et Courroies
- Courroie GT2 (cf. thingiverse)
- Roue type Galets 

#### Extrudeurs
Extrudeur direct.

Quel type de rouage ? (cf. printable on thingiverse)

#### Motors (x,y,z ; e)
4x NEMA 17

#### Chariot
- chariot basé sur U30 (similitude U20 ?)

#### Tête d'impression
- Radiateur de dissipation thermique
- buse MK8

#### Moteurs
NEMA 17

#### Visserie
M3 à M8 ++

---

### Electronique
#### DC module
Some users told about fiabilty of electrical high voltage pins...

#### Carte mère
Seems like there was multiple motherboard model...
https://fr.gearbest.com/printer-parts/pp_009630937039.html?lkid=11394067

#### Ecran tactile
https://fr.gearbest.com/printer-parts/pp_009929346133.html?lkid=11394067

#### Détecteur fin de filament
Module générique ?

#### Tête d'impression
- Thermistance (20K de base ?)
- Cartouche de chauffe 12v

#### Ventilateur
2x40mm
1x20mm

---

### Informatique
### firmware
2 firwmare :
- Screen is LCD and have is own firmware
- motherboard with marlin

### Slicers integration 
Sur Cura profil U30 ou profil Ender 3

### Custom Gcode
...


<!-- END OF CHAPTER -->


## Problèmes récurents

**Inversion des Axes XYZ :**

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse tempor, ligula sit amet rhoncus eleifend, augue purus vulputate nibh, in elementum dui metus suscipit leo. Morbi in mi nec ligula lobortis condimentum quis id lorem. Nulla sagittis diam a lacinia pharetra. Duis malesuada pulvinar leo a suscipit. 

**Extrudeur qui fonctionne à l'envers :**

Proin pretium lacus luctus, volutpat purus non, iaculis orci. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.

**Extrudeur qui retire le filament en debut de printer :**

Quisque a pretium arcu. Aliquam lobortis ullamcorper sapien, vel accumsan mi consequat non. Sed non massa ut mauris blandit condimentum sit amet eget nisi. Vestibulum accumsan ante non eros hendrerit sodales. Aliquam bibendum vel diam nec ultricies.

**Problemes d'aderence :**

Nullam orci orci, ultricies at lacus ac, egestas dignissim turpis. Suspendisse lobortis diam quis mattis eleifend. In volutpat rutrum ullamcorper. Ut at aliquet tortor. Nullam faucibus rutrum pulvinar.

**Problème de niveau :**

Nullam orci orci, ultricies at lacus ac, egestas dignissim turpis. Suspendisse lobortis diam quis mattis eleifend. In volutpat rutrum ullamcorper. Ut at aliquet tortor. Nullam faucibus rutrum pulvinar.

**Extrudeur qui claque :**

Nullam orci orci, ultricies at lacus ac, egestas dignissim turpis. Suspendisse lobortis diam quis mattis eleifend. In volutpat rutrum ullamcorper. Ut at aliquet tortor. Nullam faucibus rutrum pulvinar.

**Sous/sur-extrusion :**
Nullam orci orci, ultricies at lacus ac, egestas dignissim turpis. Suspendisse lobortis diam quis mattis eleifend. In volutpat rutrum ullamcorper. Ut at aliquet tortor. Nullam faucibus rutrum pulvinar.

**Print qui se décale :**
Verifiez vis excentrique et bonne ciruclation des axes X et Y. Moteurs éteins on doit pouvoir faire circuler à la main (meme chose pour Z, qui doit etre un peu plus lourd, voir autre chapitre).

<!-- END OF CHAPTER -->


## Evolutions possible
### Simple et bon marché
**ventilateur**

**fanduct**

**guide cable et filament**

**extrudeur**

**protection éléctrique**

**support bruits**

**buse mk9**

...

---

### Complexe et plus chère
**level automatic sensor (bltouch, touchmi)**

**Direct extrusion**

**Z double axis**

**Marlin2**

**octoprint**

**gainage des cables**

...

 
<!-- END OF CHAPTER -->

